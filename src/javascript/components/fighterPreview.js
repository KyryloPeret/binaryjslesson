import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const imgElement = createFighterImage(fighter);
  const fighterInfoElement = createFighterInfo(fighter);
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  fighterElement.append(fighterInfoElement);

  return fighterElement;
}

export function createFighterInfo(fighter) {
  const { name, attack, defense, health } = fighter;
  const fighterInfoContainer = createElement({ tagName: 'div', className: 'fighter-preview___info' });
  const fighterName = createElement({ tagName: 'span', className: 'fighter-preview___info-name' });
  fighterName.innerText = name;

  const fighterInfoList = createElement({ tagName: 'ul', className: 'fighter-preview___info-list' });
  const fighterInfoAttack = createElement({ tagName: 'li', className: 'fighter-preview___info-attack' });
  fighterInfoAttack.innerText = `Attack: ${attack}`;
  const fighterInfoDefense = createElement({ tagName: 'li', className: 'fighter-preview___info-defense' });
  fighterInfoDefense.innerText = `Defense: ${defense}`;
  const fighterInfoHealth = createElement({ tagName: 'li', className: 'fighter-preview___info-health' });
  fighterInfoHealth.innerText = `Health: ${health}`;

  fighterInfoContainer.append(fighterName);
  fighterInfoList.append(fighterInfoAttack, fighterInfoDefense, fighterInfoHealth)
  fighterInfoContainer.append(fighterInfoList);

  return fighterInfoContainer;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
