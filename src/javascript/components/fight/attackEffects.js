export function attackEffects(attacker, defender) {
    moveAttackerAnimation(attacker);
    if (!defender.block) {
        defenderBackground(defender);
    }
}

export function comboAttackEffects(attacker, defender) {
    moveAttackerAnimation(attacker);
    defenderBackground(defender);
}

function moveAttackerAnimation(attacker) {
    const fighterElement = document.getElementsByClassName(`arena___${attacker.position}-fighter`)[0];
    let margin = 30;
    if (attacker.position === 'left') {
        fighterElement.style.marginLeft = `${margin.toString()}vw`;
    } else if (attacker.position === 'right') {
        fighterElement.style.marginRight = `${margin.toString()}vw`;
    }
    setTimeout(function () {
        fighterElement.style.marginLeft = '';
        fighterElement.style.marginRight = '';
    }, 300)
}

function defenderBackground(defender) {
    const fighterElement = document.getElementsByClassName(`arena___${defender.position}-fighter`)[0];
    const fighterImg = fighterElement.getElementsByClassName(`fighter-preview___img`)[0];

    fighterImg.style.background = 'radial-gradient(red 9px, transparent)';
    fighterImg.style.borderRadius = '18%';
    setTimeout(function () {
        fighterImg.style.background = '';
        fighterImg.style.borderRadius = '';
    }, 200)
}
