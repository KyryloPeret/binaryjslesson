import {showModal} from "./modal";
import {createFighterImage, createFighterInfo} from "../fighterPreview";
import {createElement} from "../../helpers/domHelper";
import {createFighter} from "../arena";
import App from "../../app";

export function showWinnerModal(fighter) {
  // call showModal function
    showModal({title: 'Winner', bodyElement: createBodyElement(fighter), onClose: winnerModalOnClose});
}

function winnerModalOnClose() {
    removeArena()
    App.startApp();
}

function removeArena() {
    const arenaRootElement = document.getElementsByClassName('arena___root');
    arenaRootElement[0].remove();
}

function createBodyElement(fighter) {
    const bodyElement = createElement({
        tagName: 'div',
        className: `winner-modal___root`,
    });

    if (fighter.position !== undefined) {
        const imgElement = createFighter(fighter, fighter.position);
    }
    const imgElement = createFighterImage(fighter);
    if (fighter.remainingHealth !== undefined
        // && fighter.comboCount !== undefined
        // && fighter.simpleAttackCount !== undefined
    ) {
        const fighterInfo = createFighterData(fighter);
    }
    const fighterInfo = createFighterInfo(fighter);
    fighterInfo.style.removeProperty('-webkit-text-stroke-width');
    fighterInfo.style.removeProperty('-webkit-text-fill-color');
    fighterInfo.style.removeProperty('font-family');
    bodyElement.append(imgElement, fighterInfo);

    return bodyElement;
}

function createFighterData(fighter) {
    // todo display the number of attacks
    return null;
}
