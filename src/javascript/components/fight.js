import {comboAttackInterval, controls} from '../../constants/controls';
import { getRandom } from '../helpers/functionsHelper';
import {attackEffects, comboAttackEffects} from './fight/attackEffects';

export async function fight(firstFighter, secondFighter) {
  secondFighter.remainingHealth = secondFighter.health;
  secondFighter.position = 'right';
  secondFighter.isCombo = true;

  // secondFighter.comboCount = 0;
  // secondFighter.simpleAttackCount = 0;

  firstFighter.remainingHealth = firstFighter.health;
  firstFighter.position = 'left';
  firstFighter.isCombo = true;

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const keyCombinations = new Set();
    document.addEventListener('keydown', function(event) {
      switch (event.code) {
        // simple attack
        case controls.PlayerOneAttack:
          if (!event.repeat && !firstFighter.block) {
            attack(firstFighter, secondFighter, resolve);
          }
          break;
        case controls.PlayerTwoAttack:
          if (!event.repeat && !secondFighter.block) {
            attack(secondFighter, firstFighter, resolve);
          }
          break;
        // block
        case controls.PlayerOneBlock:
          firstFighter.block = true;
          break;
        case controls.PlayerTwoBlock:
          secondFighter.block = true;
          break;
        // combo-attack
        case controls.PlayerOneCriticalHitCombination[0]:
        case controls.PlayerOneCriticalHitCombination[1]:
        case controls.PlayerOneCriticalHitCombination[2]:
          if (!firstFighter.block) {
            keyCombinations.add(event.code);
            comboAttack(firstFighter, secondFighter, resolve, keyCombinations);
          }
          break;
        case controls.PlayerTwoCriticalHitCombination[0]:
        case controls.PlayerTwoCriticalHitCombination[1]:
        case controls.PlayerTwoCriticalHitCombination[2]:
          if (!secondFighter.block) {
            keyCombinations.add(event.code);
            comboAttack(secondFighter, firstFighter, resolve, keyCombinations);
          }
          break;
      }
    });
    document.addEventListener('keyup', function(event) {
      switch (event.code) {
        // block
        case controls.PlayerOneBlock:
          firstFighter.block = false;
          break;
        case controls.PlayerTwoBlock:
          secondFighter.block = false;
          break;
        // combo-attack
        case controls.PlayerOneCriticalHitCombination[0]:
        case controls.PlayerOneCriticalHitCombination[1]:
        case controls.PlayerOneCriticalHitCombination[2]:
        case controls.PlayerTwoCriticalHitCombination[0]:
        case controls.PlayerTwoCriticalHitCombination[1]:
        case controls.PlayerTwoCriticalHitCombination[2]:
          keyCombinations.delete(event.code);
          break;
      }
    });
  });
}

function comboAttack(attacker, defender, resolve, keyCombinations) {
  if (checkCombo(attacker, keyCombinations)) {
    defender.remainingHealth = defender.remainingHealth - (attacker.attack * 2);
    comboAttackEffects(attacker, defender);
    changeHealthBar(defender);
    if (defender.remainingHealth <= 0) {
      resolve(attacker);
    }
    setComboTimer(attacker);
  }
}

function checkCombo(attacker, keyCombinations) {
  if (attacker.position === 'left') {
    return !!(keyCombinations.has(controls.PlayerOneCriticalHitCombination[0])
        && keyCombinations.has(controls.PlayerOneCriticalHitCombination[1])
        && keyCombinations.has(controls.PlayerOneCriticalHitCombination[2])
        && attacker.isCombo === true
    );
  } else if (attacker.position === 'right') {
    return !!(keyCombinations.has(controls.PlayerTwoCriticalHitCombination[0])
        && keyCombinations.has(controls.PlayerTwoCriticalHitCombination[1])
        && keyCombinations.has(controls.PlayerTwoCriticalHitCombination[2])
        && attacker.isCombo === true
    );
  }
  return null;
}

function setComboTimer(fighter) {
  fighter.isCombo = false;
  let comboTimer = comboAttackInterval;

  const healthBarElement = document.getElementById(`${fighter.position}-fighter-combo-timer`);
  // healthBarElement.style.-webkit-text-fill-color = 'white';
  healthBarElement.style.color = 'white';
  healthBarElement.style.fontSize = '45px';
  healthBarElement.innerText = comboTimer.toString();
  --comboTimer

  let timer = setInterval(function () {
    if (!comboTimer <= 0) {
      healthBarElement.innerText = comboTimer.toString();
    } else {
      clearInterval(timer);
      fighter.isCombo = true;
      healthBarElement.innerText = 'Combo!';
      healthBarElement.style.color = 'black';
      healthBarElement.style.fontSize = '26px';
    }
    --comboTimer;
  }, 1000)
}

function attack(attacker, defender, resolve) {
  defender.remainingHealth = defender.remainingHealth - getDamage(attacker, defender);
  attackEffects(attacker, defender);
  changeHealthBar(defender);
  if (defender.remainingHealth <= 0) {
    resolve(attacker);
  }
}

function changeHealthBar(fighter) {
  const healthBarElement = document.getElementById(`${fighter.position}-fighter-indicator`);
  const healthPercent = Math.round((fighter.remainingHealth / fighter.health) * 100);
  healthBarElement.style.width = healthPercent + '%';
  console.log(healthBarElement.style.width)
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage < 0 || defender.block === true) {
    return 0;
  }
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  return fighter.attack * criticalHitChance();
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense * dodgeChance();
}

function dodgeChance() {
  return getRandom({min: 1, max: 2});
}

function criticalHitChance() {
  return getRandom({min: 1, max: 2});
}
